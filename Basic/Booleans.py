
age = 35

if 30 <= age <= 39:
    print("Diese Frau ist in ihren 30-ern")

age = 20

if age < 30 or age >= 40:
    print("Diese Frau ist nicht in ihren 30-ern")

# Booleans
x = True
print(x)

y = False
print(y)

above_30 = age >= 30
print(above_30)



if True:
    print("if-Abfrage wurde ausgeführt")


print(True and True)
print(True and False)
print(False and True)
print(False and False)


print(True or True)
print(True or False)
print(False or True)
print(False or False)


country = "US"
age = 20

if (country == "Us" and age >= 21) or (country != "US" or age >= 18):
    print("Diese Person darf Alkohol trinken")