# While-Schleife
counter = 0

while counter < 10:
    print(counter)
    counter = counter + 1

# For-Schleife

for i in range (0, 10):
    print(i)

Liste = (1, 2, 3)
for i in Liste:
    print(i)

# Schleifen (break und contiune)

for i in range(0, 10):
    if i == 3:
        continue
    print(i)

for i in range(0, 10):
    if i == 3:
        break
    print(i)


