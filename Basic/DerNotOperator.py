names = ('Max', 'Nadine')

if 'Moritz' not in names:
    print('Moritz ist nicht in der Liste enthalten')
else:
    print('Moritz ist in der Liste enthalten')

# Ohne not Operator
age = 35

if age >= 30:
    print('ausgeführt')


# Mit not Operator
age = 20

if not age >= 30:
    print('ausgeführt')

if age < 30:
    print('ausgeführt')

