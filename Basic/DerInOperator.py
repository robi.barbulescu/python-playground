# Listen
students = ('Max', 'Monika', 'Erik', 'Franziska')

print('Max' in students)
print('Moritz' in students)


if 'Monika' in students:
    print('Ja, die Monika studiert hier!')
else:
    print('Nein, die Monika studiert hier nicht!')

if 'Moritz' in students:
    print('Ja der Moritz sudiert hier!')
else:
    print('Nein, der Moritz studiert hier nicht!')

# Strings
sentence = 'Ja, die Monika studiert hier!'

if '?' in sentence:
    print('Ja')
else:
    print('Nein')