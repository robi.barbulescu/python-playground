import random

part1 = ("Merkur", "Venus", "Erde", "Mars", "Jupiter", "Saturn", "Uranus", "Neptun")
part2 = ('ist', 'war', 'wird', 'ist nicht', 'war nicht', 'wird nicht')
part3 = ('der grösste', 'der schwerste', 'der kleinste', 'der leichteste')
part4 = ('Planet.', 'Himmelskörper.')

best_words = (part1, part2, part3, part4)

sentence = []

for part in best_words:
    r = random.randint(0, len(part) - 1)
    sentence.append(part[r])

print(' '.join(sentence))
















