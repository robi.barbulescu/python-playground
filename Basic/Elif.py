# else and if
currency = '¥'

if currency == '€':
    print('Euro')
else:
    if currency == '$':
        print('Dollar')
    else:
        if currency == '¥':
            print('Yen')
        else:
            print('Sonstige')

# elif
currency = '¥'

if currency == '€':
    print('Euro')
elif currency == '$':
    print('Dollar')
elif currency == '¥':
    print('Japanischer Yen')
else:
    print('Sonstige')