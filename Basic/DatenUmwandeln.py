a = "5"
b = "6"

print(a + b)
print(int(a) + int(b))

# string -> Kommazahl

a = "5.5"
b = "6.6"
print(float(a) + float(b))

age = 21
print("Ich bin " + str(age) + " Jahre Alt")

# Liste -> String

students = ["Max", "Monika", "Erik", "Franziska"]
print(", ".join(students))

students_as_string = ", ".join(students)
print("An unserer Uni Studieren:" + students_as_string)

# String -> Liste

i = "Max, Monika, Erik, Franziska"
print(i.split(", "))


a = "Ich bin ein Satz mit vielen wörtern"
print(a.split(" "))
print(len(a.split(" ")))


